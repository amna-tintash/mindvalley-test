package mvalley.amnamirza.android.test;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import UtilityClasses.ConnectionDetector;
import pl.droidsonroids.gif.GifImageView;

import static android.R.id.list;
import static android.content.ContentValues.TAG;
import static java.lang.Thread.sleep;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    static ArrayList<CardView> dataSet;
    String file;
    Context ctx;
    View view;
    boolean flag = true;
    ImageView bg_down;
    GifImageView gif_down;


    public CustomAdapter(ArrayList<CardView> dataSet, Context ctx) {
        this.dataSet = dataSet;
        this.ctx = ctx;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Name;
        TextView User_name;
        TextView Likes;
        ImageView profile_img;
        RelativeLayout profile_bg;
        ImageView download;
        Button profile_view;
        GifImageView gif;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.Name = (TextView) itemView.findViewById(R.id.tv_card_view_profile_name);
            this.User_name = (TextView) itemView.findViewById(R.id.tv_card_view_profile_user_name);
            this.Likes = (TextView) itemView.findViewById(R.id.tv_card_view_profile_likes);
            this.profile_img = (ImageView) itemView.findViewById(R.id.iv_card_view_profile_pic);
            this.profile_bg = (RelativeLayout) itemView.findViewById(R.id.rl_card_view_profile_bg);
            this.download = (ImageView) itemView.findViewById(R.id.iv_card_view_download);
            this.profile_view = (Button) itemView.findViewById(R.id.btn_card_view_);
            this.gif = (GifImageView) itemView.findViewById(R.id.gif_pic_preview);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_card_view, parent, false);
        view.setOnClickListener(DetailScreen.myOnClickListener);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Context context_img = holder.profile_img.getContext();
        final Context context_bg = holder.profile_bg.getContext();
        final Context context_profile_view = holder.profile_view.getContext();
        final Context context_profile_bg_download = holder.download.getContext();


        TextView Name = holder.Name;
        TextView User_name = holder.User_name;
        TextView Likes = holder.Likes;
        Button profile_view = holder.profile_view;

        final ImageView profile_img = holder.profile_img;
        final RelativeLayout profile_bg = holder.profile_bg;
        final ImageView bg_download = holder.download;
        final GifImageView gif_load = holder.gif;
        bg_down = bg_download;
        gif_down = gif_load;


        Name.setText(dataSet.get(position).getName());
        User_name.setText(dataSet.get(position).getUser_name());
        Likes.setText(dataSet.get(position).getLikes());
        file = dataSet.get(position).getFile();
        file = file.replaceAll("\\\\", "");

        final String profile_link = dataSet.get(position).getProfile_pic_str();
        final String bg_link = dataSet.get(position).getProfile_bg_str();
        final String profile_view_link = dataSet.get(position).getView_profile();

        Picasso.with(context_img)
                .load(profile_link)
                .into(holder.profile_img);
        Picasso.with(context_bg)
                .load(bg_link)
                .into(new Target() {


                    @Override
                    public void onPrepareLoad(Drawable arg0) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom arg1) {
                        // TODO Auto-generated method stub
                        profile_bg.setBackground(new BitmapDrawable(context_bg.getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(context_bg, "Failed Loading Background", Toast.LENGTH_SHORT).show();
                    }
                });

        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String address = profile_link; //"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=128&w=128&s=622a88097cf6661f84cd8942d851d9a2";
                Intent intent = new Intent(context_img, PicPreview.class);
                intent.putExtra("name", address);
                intent.putExtra("user_name", holder.User_name.getText());
                context_img.startActivity(intent);


            }

        });
        profile_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = bg_link; //"https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=1881cd689e10e5dca28839e68678f432";
                Intent intent = new Intent(context_img, PicPreview.class);
                intent.putExtra("name", address);
                intent.putExtra("user_name", holder.User_name.getText());
                context_img.startActivity(intent);

                //storeImage(bmp_bg,holder);
            }
        });


        bg_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = holder.User_name.getText().toString();
                final String address = bg_link;
                bg_download.setVisibility(View.GONE);
                gif_load.setVisibility(View.VISIBLE);
                flag = true;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        bg_download.setVisibility(View.VISIBLE);
                        gif_load.setVisibility(View.GONE);
                        if(flag){
                            Bitmap bmp = getBitmap(address);
                            storeImage(bmp, name);
                            Toast.makeText(context_profile_bg_download,"Background downloaded successfully.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 2000);
            }
        });

        gif_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context_profile_bg_download, "Downloading cancelled", Toast.LENGTH_SHORT).show();
                bg_download.setVisibility(View.VISIBLE);
                gif_load.setVisibility(View.GONE);
                flag = false;
            }
        });

        profile_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = holder.User_name.getText().toString();
                String timeStamp = new SimpleDateFormat("mmss").format(new Date());
                try {
                    File mediaStorageDir = new File("/sdcard/MValley/"
                            + name);
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {
                            return;
                        }
                    }

                    File myFile = new File("/sdcard/MValley/" + name + "/" + name + timeStamp + ".txt");
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append(file);
                    myOutWriter.close();
                    fOut.close();
                    Toast.makeText(context_profile_view,
                            "File Downloaded Successfully ",
                            Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(context_profile_view, e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(context_profile_view, WebViewer.class);
                intent.putExtra("address", profile_view_link);
                context_profile_view.startActivity(intent);
            }
        });


    }

    public static Bitmap getBitmap(String url) {
        try {
            //Storing User Data in file in Device
            InputStream is = (InputStream) new URL(url).getContent();
            Bitmap d = BitmapFactory.decodeStream(is);
            is.close();

            return d;
        } catch (Exception e) {
            return null;
        }
    }

    private void storeImage(Bitmap image, String fileName) {
        File pictureFile = getOutputMediaFile(fileName);
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");
            return;
        }
        try {
            //Storing Images in Device
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    private File getOutputMediaFile(String fileName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File("/sdcard/MValley/"
                + fileName);


        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("mmss").format(new Date());
        File mediaFile;
        String mImageName = fileName + timeStamp + ".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }


}






