package mvalley.amnamirza.android.test;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import pl.droidsonroids.gif.GifImageView;

import static android.content.ContentValues.TAG;
import static java.lang.Thread.sleep;

public class PicPreview extends AppCompatActivity {
    ImageView img;
    Button btn;
    String address;
    String name;
    GifImageView gif;
    boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_preview);

        //Initializing
        img=(ImageView)findViewById(R.id.iv_profile_pic);
        btn=(Button)findViewById(R.id.btn_profile_download);
        gif = (GifImageView) findViewById(R.id.gif_pic_preview_download);
        address= getIntent().getStringExtra("name");
        name=getIntent().getStringExtra("user_name");
        final Bitmap bmp=getBitmap(address);
        img.setImageBitmap(bmp);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
                gif.setVisibility(View.VISIBLE);
                btn.setVisibility(View.GONE);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        btn.setVisibility(View.VISIBLE);
                        gif.setVisibility(View.GONE);
                        if(flag){
                            storeImage(bmp,name);
                            Toast.makeText(PicPreview.this, "Downloaded Succesfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 2000);

            }
        });

        gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(PicPreview.this, "Downloading cancelled", Toast.LENGTH_SHORT).show();
                btn.setVisibility(View.VISIBLE);
                gif.setVisibility(View.GONE);
                flag = false;
            }
        });

    }

    public static Bitmap getBitmap(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Bitmap d = BitmapFactory.decodeStream(is);
            is.close();

            return d;
        } catch (Exception e) {
            return null;
        }
    }

    private void storeImage(Bitmap image,String fileName) {
        File pictureFile = getOutputMediaFile(fileName);
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }
    private  File getOutputMediaFile(String fileName){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File("/sdcard/MValley/"
                +fileName);

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("mmss").format(new Date());
        File mediaFile;
        String mImageName=fileName+ timeStamp +".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }
}
