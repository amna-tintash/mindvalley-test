package mvalley.amnamirza.android.test;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import UtilityClasses.ConnectionDetector;
import UtilityClasses.GetDataFromWebservice;

public class DetailScreen extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<CardView> data;
    static View.OnClickListener myOnClickListener;
    ConnectionDetector cd;

    private SwipeRefreshLayout swipeContainer;
    String swipeCheck = "noSwipe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);

        // Initializing
        cd=new ConnectionDetector(this);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.activity_detail_screen);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        data = new ArrayList<>();

        //Creating App Folder In SdCard
        File wallpaperDirectory = new File("/sdcard/MValley/");
        wallpaperDirectory.mkdirs();

        //Checking Internet Connectivity
        if(cd.isConnectingToInternet())
        {
            new ReadingData().execute("http://pastebin.com/raw/wgkJgazE");
        }
        else{
            Toast.makeText(this, "You are not connected to Internet", Toast.LENGTH_SHORT).show();
        }

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeCheck = "swipe";
                new ReadingData().execute("http://pastebin.com/raw/wgkJgazE");
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    //Animation for Recycler View
    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(700);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    //Clearing data for pull to refresh
    public void clear() {
        data.clear();
        adapter.notifyDataSetChanged();
    }

    //Async Task to read data from server
    private class ReadingData extends AsyncTask<String,String,String>{

        private ProgressDialog pdia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!(swipeCheck.equals("swipe"))) {
                //Progress to show reading data process
                pdia = new ProgressDialog(DetailScreen.this);
                pdia.setMessage("Loading Data...");
                pdia.show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return GetDataFromWebservice.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            if(!(swipeCheck.equals("swipe"))) {
                pdia.dismiss();
                recyclerView.startAnimation(inFromLeftAnimation());
            }
            else {
                clear();
                swipeContainer.setRefreshing(false);
                recyclerView.startAnimation(inFromLeftAnimation());
            }

            JSONArray  mainArray;
            JSONObject arrayObject;
            JSONObject user;
            JSONObject profileImage;
            JSONObject links;
            JSONObject urls;

            try {
                mainArray = new JSONArray(result);
                int size = mainArray.length();
                for(int i=0;i<size;i++){
                    arrayObject = mainArray.getJSONObject(i);
                    String likes = arrayObject.getString("likes");
                    user = arrayObject.getJSONObject("user");
                    String name = user.getString("name");
                    String username = user.getString("username");
                    profileImage = user.getJSONObject("profile_image");
                    String imageLink = profileImage.getString("large");
                    links = user.getJSONObject("links");
                    String userLink = links.getString("html");
                    urls = arrayObject.getJSONObject("urls");
                    String regularLink = urls.getString("regular");

                    data.add(new CardView(name,username,likes,imageLink,regularLink,userLink,mainArray.getJSONObject(i).toString()));
                }
                adapter = new CustomAdapter(data,DetailScreen.this);

                recyclerView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
