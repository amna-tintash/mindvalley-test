package mvalley.amnamirza.android.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import UtilityClasses.ConnectionDetector;

public class WebViewer extends AppCompatActivity {

    WebView broswer;
    String address;
    ConnectionDetector cd;
    Toast t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        address=getIntent().getStringExtra("address");

        cd = new ConnectionDetector(this);
        t = Toast.makeText(this,"You don`t have any network access now",Toast.LENGTH_LONG);

        broswer=(WebView)findViewById(R.id.wv_webview);
        broswer.setWebViewClient(new MyWebViewClient());

        if(!(cd.isConnectingToInternet())){
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        }
        else{
            broswer.loadUrl(address);
        }

        broswer.loadUrl(address);
    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(!(cd.isConnectingToInternet())){
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            }
            else {
                view.loadUrl(url);
            }
            return true;
        }
        @Override
        public void onPageFinished(WebView view, final String url) {

        }
    }
}
