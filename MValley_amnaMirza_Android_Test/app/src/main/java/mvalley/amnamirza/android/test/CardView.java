package mvalley.amnamirza.android.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONArray;

public class CardView  {


    String name;
    String user_name;
    String likes;
    String profile_pic_str;
    String profile_bg_str;
    String view_profile;
    String file;

    public CardView() {
    }


    public CardView(String name, String user_name, String likes, String profile_pic_str, String profile_bg_str, String view_profile, String file) {
        this.name = name;
        this.user_name = user_name;
        this.likes = likes;
        this.profile_pic_str = profile_pic_str;
        this.profile_bg_str = profile_bg_str;
        this.view_profile = view_profile;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }


    public String getProfile_pic_str() {
        return profile_pic_str;
    }

    public void setProfile_pic_str(String profile_pic_str) {
        this.profile_pic_str = profile_pic_str;
    }

    public String getProfile_bg_str() {
        return profile_bg_str;
    }

    public void setProfile_bg_str(String profile_bg_str) {
        this.profile_bg_str = profile_bg_str;
    }

    public String getView_profile() {
        return view_profile;
    }

    public void setView_profile(String view_profile) {
        this.view_profile = view_profile;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
